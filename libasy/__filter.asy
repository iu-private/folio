T[] filter(bool predicate(T x), T arr[]) {
  T result[];
  for (var x: arr)
    if (predicate(x))
      result.push(x);
  return result;
}
