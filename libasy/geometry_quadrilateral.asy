import useful;
import geometry;

/* There is no inner structure in arbitary quadrilateral, save that
 * all points must be distinct, so point variables are neither
 * restricted nor private. But you are are still encouraged to use
 * more specialized constructors and common transformations:
 *
 *  - shift
 *  - rotate
 *  - scale
 *
 */
struct quadrilateral {
	point A, B, C, D;
	void operator init(point A, point B, point C, point D) {
		this.A = A;
		this.B = B;
		this.C = C;
		this.D = D;
	}
	point point(int i) {
		if (i == 0) return A;
		if (i == 1) return B;
		if (i == 2) return C;
		if (i == 3) return D;
		assert(false, "rectangle.point(int): invalid argument " + (string) i);
		return (0, 0);
	}
};

quadrilateral operator *(transform t, quadrilateral q) {
	return quadrilateral(t * q.A, t * q.B, t * q.C, t * q.D);
};

quadrilateral rectangle(point A, point C) {
	return quadrilateral(A, (A.x, C.y), C, (C.x, A.y));
}

quadrilateral rectangle(real proportion = 1) {
	return rectangle((0, 0), (proportion, 1));
}
quadrilateral square = rectangle();

/* Trapzium is defined by its heigh/base ratio and two angles,
 * adjanced to base.
 */
quadrilateral trapezium(real phi, real psi = phi, real h = 0.3) {
	point A = (0, 0);
	point B = (h / tan(phi), h );
	point C = (1 - h / tan(psi), h);
	point D = (1, 0);
	return quadrilateral(A, B, C, D);
}

quadrilateral parallelogram(real alpha, real proportion = 1) {
	point A = (0, 0);
	point B = (cos(alpha), sin(alpha));
	point C = (proportion + cos(alpha), sin(alpha));
	point D = (proportion, 0);
	return quadrilateral(A, B, C, D);
}

quadrilateral inquadrilateral(real alpha, real beta, real gamma,
                              real delta = 1) {
	real sum = (alpha + beta + gamma + delta);
	point getpoint(real frac) {
		real phi = 2 * pi * frac;
		return (cos(phi), sin(phi));
	};
	point A = getpoint(0);
	point B = getpoint(alpha/sum);
	point C = getpoint((alpha + beta)/sum);
	point D = getpoint((alpha + beta + gamma)/sum);
	return quadrilateral(A, B, C, D);
}

void draw(quadrilateral r, string labels = "ABCD", pen p = currentpen) {
	unravel r;
	draw(A -- B -- C -- D -- cycle);
	if (labels == "")
		return;
	for (int i = 0; i != 4; ++i) {
		int iopposite = (i + 2) % 4;
		pair align = unit(r.point(i) - r.point(iopposite));
		label(at(labels, i), r.point(i), align);
	}
}
