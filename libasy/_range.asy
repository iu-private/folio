import useful;
/* Structure, that represent limit of real numbers range.
 *
 * Fields 'value' and 'finite' are connected, so they are both
 * restricted and 'finine' is inferred from 'value' during
 * construction, depending whether 'value' is ±infinity.
 *
 * Other fields: 'included' -- whether limit is inclusive or exclusive
 * and 'label' for limit on number axis are independant, so it is okay
 * to modify them, but using structure in read-only way is still
 * encouraged.
 */
struct limit {
  restricted real value = 0.0;
  restricted bool finite = true;
  bool included;
  Label label;
  void operator init(real value, bool included, Label label = null) {
    this.value = value;
    this.included = included;
    this.finite = value != infinity && value != -infinity;
    this.label = label;

    /* Create default label for finite limit. Default values is opinionated
     * thing by defintion.
     *
     * I consider scriptstyle label more appealing.
     */
    if (this.finite && this.label == null)
      this.label = sprintf("$\scriptstyle {0}$", (string)value);
  }
};
typedef limit T;
include __uniq;
include __filter;

/* Again, very opinionated defintion of compraison operators.
 * For my applications it is more convenient to compare exclusively
 * on 'value' field, ignoring 'included' and, sure, 'label' fields.
 */
bool operator < (limit x, limit y) { return x.value < y.value; }
bool operator > (limit x, limit y) { return x.value > y.value; }
bool operator <= (limit x, limit y) { return x.value <= y.value; }
bool operator >= (limit x, limit y) { return x.value >= y.value; }
bool operator == (limit x, limit y) { return x.value == y.value; }
bool operator != (limit x, limit y) { return x.value != y.value; }

/* Functions with short and expressive names, to create limits. */
limit in(real value, Label label = null) {
  return limit(value, true, label);
}
limit out(real value, Label label = null) {
  return limit(value, false, label);
}
limit operator cast(real value) { return out(value); };

/* Structure, that represent continuous range of real numbers. */
struct range {
  restricted limit left, right;
  restricted bool finite;
  void operator init(limit left, limit right) {
    assert(left <= right, "range: left is greater then right");
    this.left = left;
    this.right = right;
    this.finite = left.finite && right.finite;
  }
}

/* Operator for creating range from pair of limits is really
 * convenient. Choice between '..' and '--' in favor of last is
 * due operator..(int, int) do exist in Asymptote core. (But unfortunately,
 * I fail to understand its functionality).
 *
 * Due rather complex type casting rules, at least following overloads
 * are required, so, for example, '10..15' typechecks as 'range'.
 */
range operator--(limit left, limit right) { return range(left, right); }
range operator--(real x, real y) { return range(out(x), out(y)); }
range operator--(int left, limit right) { return range(out(left), right); }
range operator--(limit left, int right) { return range(left, out(right)); }
