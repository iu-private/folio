Generic functions
=================

Asymptote does not support polymorphic functions. It means, that if you need
to perform some algorithm on array of ``int`` and array of some user-defined
structures, writing two separate functions is required.

In terms of conventional programming languages, there is no equivalent for
following pieces of code:

.. code::

        template <T> void foo(T *array, size_t N) // C++
        foo :: (Ord a) => [a] -> [a] // Haskell

But it can be emulated with use of typedefs and include statement, like following:

.. code::

        typedef <datatype> T;
        include __<function>;

By convention, generic parameter is named ``T``, filename to include have two
leading underscores, prepended by function name. This technics works due the
fact, that typedefs are expanded when function is defined, not when function is
called.

Function **uniq**
-----------------

.. code-block:: none

        T[] uniq(T array[], bool eq(T, T) = operator ==)

Return new array with adjanced duplicates in sense of ``eq`` function removed.

.. note::

        Unless redefined, equality operator for structures checks for reference
        equality, not structural. It may be not what you expect.

Function **filter**
-------------------

.. code-block:: none

        T[] filter(bool predicate(T x), T array[])

Return new array with only those elements, for which ``predicate`` functions
returns true boolean value.
