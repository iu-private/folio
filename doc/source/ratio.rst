===============
 Ratio library
===============

Asymptote language does not provide rational numbers built in, this
library provides :cpp:class:`ratio` structure to fix that limitation.

.. cpp:class:: ratio

   Structure representing rational number. It can be constructed with
   ``operator ::`` or implicitly casted from integer value.

.. cpp:member:: int ratio::numerator
		int ratio::denumerator

   Rational number is represented with two integer values
   *numerator* and *denumerator*, with
   following invariants maintained by constructor:

      * ``denumerator > 0``
      * ``greatest-common-divisor(numerator, denumerator) == 1``

   .. note::

      Since Asymptote does not provide long arithmetics, both
      numerator and denominator are built-in integers, and are subject
      to integer overflow. You will notice if you encounter
      this limitation.

.. cpp:function:: ratio operator :: (int n, int m)
		  ratio operator -(ratio r)
		  ratio operator +(ratio r1, ratio r2)
		  ratio operator -(ratio r1, ratio r2)
		  ratio operator *(ratio r1, ratio r2)
		  ratio operator /(ratio r1, ratio r2)
		  ratio operator ^(ratio r, int n)
		  bool operator == (ratio r1, ratio r2)
		  bool operator != (ratio r1, ratio r2)
		  bool operator <= (ratio r1, ratio r2)
		  bool operator < (ratio r1, ratio r2)
		  bool operator >= (ratio r1, ratio r2)
		  bool operator > (ratio r1, ratio r2)

   These operators behave exactly as you may expect. See code example.

   .. code:: c++

	     import libasy2;
	     unravel ratio;
	     ratio x = 1 :: 2;
	     ratio y = 1 :: 3;
	     ratio z = 5 :: 10;
	     x == z
	     x + y == 5 :: 6
	     x > y
	     y <= x
	     x * z == 1 :: 4
	     x ^ 3 == 1 :: 8
