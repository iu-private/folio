# Copyright (C) 2017 Dmitry Bogatov
#
# This file is part of libasy2.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    asytest
    ~~~~~~~

    Extract examples source code from documentation and generate
    testsuite for libasy2 modules.
"""

from sphinx.builders import Builder
import docutils.nodes as nodes
from sphinx.util.osutil import ensuredir, os_path
from os import path

def write_literal_block(f, block):
    for line in block.astext().split('\n'):
        if line.endswith(';'):
            f.write(line + '\n')
        else:
            f.write("assert({0}, '{0}');\n".format(line))

class AsyTest(Builder):
    name = 'asytest'
    out_suffix = '.asy'

    def init(self):
        pass

    def get_outdated_docs(self):
        return self.env.found_docs

    def get_target_uri(self, docname, typ=None):
        return ''

    def prepare_writing(self, docnames):
        pass

    def write_doc(self, docname, doctree):
        outfilename = '{0}/{1}.asy'.format(self.outdir, docname)
        ensuredir(path.dirname(outfilename))
        with open(outfilename, 'w') as f:
            for lit in doctree.traverse(nodes.literal_block):
                write_literal_block(f, lit)

    def finish(self):
        pass

def setup(app):
    app.add_builder(AsyTest)
