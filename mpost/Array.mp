%% Copyright 2017 Dmitry Bogatov <KAction@gnu.org>
%% 
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>. 

% This library provides immitation of perl-like arrays, and crude
% implementation of some basic array algorithms. For example,
%
%       Array.new arr;
%       Array.push arr (10);
%       Array.push arr (-1);
%       Array.push arr (12);
%       show #arr;      % 3,  size of array
%       show arr[3];    % 12, arrays are 1-based
%       Array.sort arr; % sort in ascending order
%       show arr[1];    % (-1), the smallest
%       show arr[3];    % 12, the biggest
%       push arr (12);  % now 4 elements
%       Array.uniq arr; % again 3 elements, duplicates are removed.
%       Array.pop arr;  % now only two: -1 and 10 rest
%       Array.print arr % "arr: -1 10"

if known Array._libversion: endinput; fi;
Array._libversion := 1;

input etc

vardef Array.new.@# =
  numeric @#[];
  #.@# := 0;
enddef;

vardef Array.push.@#(expr value) = 
  local(numeric) len := #.@#;
  @#[len + 1] := value;
  #.@# := len + 1;
enddef;

vardef Array.pop.@# =
  local(numeric) len := #.@#;
  if len = 0: 
    error ("Array.pop: empty array " & str(@#));
  fi;

  #.@# := len - 1;
enddef;
 
vardef Array.delete.@#(expr ix) = 
  local(numeric) len := #.@#;
  if ix >= len: 
    error("Array.delete: index " & decimal(ix) & "is too big.");
  fi;
  for i = ix upto len - 1:
    @#[i] := @#[i + 1];
  endfor;
  Array.pop @#;
enddef;

vardef Array.print.@# =
  local(string) s := str @# & ": ";
  for i = 1 upto #.@#:
    s := s & " " & decimal(@#[i]);
  endfor;
  show s;
enddef;

vardef Array.sort.@# = 
  local(numeric) len := #.@#;
  save i, j;

  for i = 1 upto len:
    for j = 1 upto len - i:
      if @#[j] >= @#[j + 1]:
        swap(@#[j], @#[j+1]);
      fi;
    endfor;
  endfor;
enddef;

vardef Array.uniq.@# = 
  local(numeric) i := 1;

  forever:
    exitif (i >= #.@#);
    if @#[i] = @#[i + 1]:
      Array.delete @# (i);
    else:
      i := i + 1;
    fi;
  endfor;
enddef;
