import useful;
import geometry;
import graph;
size(10cm);

xaxis("$x$", RightTicks(Step=1), EndArrow);
yaxis("$y$", LeftTicks(Step=3), EndArrow);

draw(graph(new real(real x) { return x^2 - 5x; }, 0, 6.9));
draw(graph(new real(real x) { return -x^2 - 7x; }, -8, 0));
path p = (-8, -6.25) -- (6.9, -6.25);
draw(p, linewidth(1.1));
label("$y = -6.25$", (-8, -6.25) -- (0, -6.25));

path p = (-8, 12.25) -- (6.9, 12.25);
draw(p, linewidth(1.1));
label("$y = 12.25$", (0, 12.25) -- (6.9, 12.25), N);
