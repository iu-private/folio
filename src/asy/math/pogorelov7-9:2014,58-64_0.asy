import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), SW);
defpoint("B", (1, 2), NW);
defpoint("C", (4, 2), NE);
defpoint("D", (5, 0), SE);
draw(A -- B -- C -- D -- cycle);
draw(B -- D);
draw(segment(A, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(C, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(C, D), marker = StickIntervalMarker(i = 1, n = 2));
markrightangle(A, B, D);
markangle("$\alpha$", B, D, A);
markangle("$\alpha$", C, D, B);
markangle("$\alpha$", D, B, C);
