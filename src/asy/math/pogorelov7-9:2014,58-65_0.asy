import useful;
import geometry;
import graph;
size(10cm);


defpoint("A", (0, 1), N);
defpoint("B", (3, 2), N);
point C = (-1,0);
point D = (4, 0);

draw(C -- D);
label("$a$", C, S);
defaltitude("H", triangle(A, C, D).vertex(1));
defaltitude("K", triangle(B, C, D).vertex(1));
draw(A -- B);
defpoint("M", (A + B)/2, N);
draw(segment(A,B), marker = StickIntervalMarker(n = 2));
defaltitude("R", triangle(M, C, D).vertex(1));
draw(segment(H,K), marker = StickIntervalMarker(n = 1));
