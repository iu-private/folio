import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

var f = new real (real x) { return -0.5 x^2 - 2x + 1; };
var f1 = new real (real x) { return 3; };
var f2 = new real (real x) { return 3 - 4x; };
var f3 = new real (real x) { return 9 - 6x; };

size(10cm);

xaxis("$x$", RightTicks(Step=1), EndArrow);
yaxis("$y$", RightTicks(Step=3), EndArrow);
real K = 4;
scale(Linear(1.0), Linear(1/K));
draw(graph(f, -5, 4), linewidth(1));
draw(graph(f1, -5, 4));
draw(graph(f2, 0, 4));
draw(graph(f3, 1, 4));
draw(graph(new real (real x) { return 5; }, 1, 4.5), white);

for (real x = 0; x <= 3; x += 0.08) {
  real y1 = f2(x);
  real y2 = min(f1(x), f3(x));
  draw((x, y1/K) -- (x, y2/K), dotted);
}
