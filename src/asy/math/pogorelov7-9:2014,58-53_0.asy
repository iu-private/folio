import useful;
import geometry;
import graph;
size(10cm);


deftriangle("MNK", triangle(radians(30.0), radians(70.0)));
line M_ = line(M, M + (N - K));
line N_ = line(N, N + (M - K));
line K_ = line(K, K + (M - N));
deftriangle("ABC",
            intersectionpoint(M_, N_),
            intersectionpoint(M_, K_),
            intersectionpoint(N_, K_), draw = false);
draw(ABC, dashed + linewidth(1.2));
markangle(M, K, N, radius = markangleradius/2);
markangle(K, M, B, radius = markangleradius/2);
markangle(N, M, K, radius = markangleradius/1.5, n = 2);
markangle(B, K, M, radius = markangleradius/1.5, n = 2);
