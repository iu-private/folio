import useful;
import geometry;
import graph;
size(10cm);

xaxis("$x$", RightTicks(Step=3), EndArrow);
yaxis("$y$", RightTicks(Step=3), EndArrow);

draw(graph(new real(real x) { return x*x + 7x + 6; }, -6, -0.9));
draw(graph(new real(real x) { return x*x + 15x + 54;}, -9.2, -6));
for (real y: new real [] { 0, -2.25 }) {
  path p = (-9, y) -- (0.1, y);
  draw(p, linewidth(1.1));
  label("y = " + string(y), p);
};
