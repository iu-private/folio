import geometry;
import useful;
size(5cm);

pair B = (1.5, 1.5);
pair C = (1.5, -1.5);
pair D = (-1.5, -1.5);
pair A = (-1.5, 1.5);
pair Q = C + unit(C - D);

draw(A -- B -- C -- D --cycle);
draw(C -- Q -- B);
label("$A$", A, W);
label("$B$", B, E);
label("$C$", C, S);
label("$D$", D, W);
label("$Q$", Q, S);

point V = intersectionpoint(altitude(triangle(C, B, Q).vertex(1)), line(B,Q));
label("$V$", V, NE);
draw(C -- V);
draw(orth(V, C, B, scale = 0.3));

