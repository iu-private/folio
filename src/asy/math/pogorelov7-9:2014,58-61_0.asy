import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), SW);
defpoint("B", (1, 2), NW);
defpoint("C", (4, 2), NE);
defpoint("D", (5, 0), SE);
draw(A -- B -- C -- D -- cycle);
draw(segment(A, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(C, D), marker = StickIntervalMarker(i = 1, n = 2));
markangle("$\alpha$", D, A, B);
markangle("$\alpha$", C, D, A);
markangle("$\alpha + 40\degree$", B, C, D, n = 2);
