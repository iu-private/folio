import useful;
size(10cm);

void label_range(Label l, real x1, real x2) {
  label(l, (x1, 0) -- (x2, 0), N);
}

void select_range(real x1, real x2) {
  draw((x1 + 0.06, 0) -- (x2 - 0.06, 0), linewidth(2));
}

draw((-3, 0) -- (2, 0), EndArrow);
mark_point(1, exclude = true);
mark_point(-1);
mark_point(-2);
label_range("\huge{}+", 1, 2);
label_range("\huge --", -1, 1);
label_range("\huge +", -2, -1);
label_range("\huge --", -3, -2);
select_range(-3.1, -2);
select_range(-1, 1);
