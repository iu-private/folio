import useful;
import geometry;
import graph;
size(10cm);

draw(scale(3.5) * unitcircle);
draw(shift((3.5, 0)) * scale(4) * unitcircle);
defpoint("O", (0, 0), N);
defpoint("A", (3.5, 0), E);
fill(shift(O) * scale(0.07) * unitcircle);
fill(shift(A) * scale(0.07) * unitcircle);
