import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", triangle(radians(66), radians(84)));
circle c = circle(A, B, C);
draw(c);
path a = angle(B, C , A);
draw(a);
label("$66\degree$", a);
path a = angle(C, A , B);
draw_angle(C, A, B, repeat = 2);
draw(a);
label("$84\degree$", a);
