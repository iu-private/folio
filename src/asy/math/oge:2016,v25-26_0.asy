import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

real alpha = asin(7/25) + asin(17/25);
real proportion = 1.1;
quadrilateral tr = parallelogram(alpha, proportion);
draw(tr);
unravel tr;

draw(A -- C);
circle c = incircle(A, B, C);
draw(c);
defpoint("O", c.C, W + 2N);
defaltitude("T", triangle(A, O, C).vertex(2));
defaltitude("H", triangle(A, O, D).vertex(2));
draw(A -- O -- T -- cycle);
markangle(T, A, O);
markangle(O, A, B);
defpoint("K", intersectionpoint(line(A, O), line(B, C)), N);
draw(A -- K);
defaltitude("R", triangle(A, O, B).vertex(2));
defpoint("L", intersectionpoint(line(O, H), line(B, C)), N);
draw(O -- L);
markrightangle(O, L, K);
markangle(A, O, H, n = 2, radius = markangleradius/2);
markangle(K, O, L, n = 2, radius = markangleradius/2);
