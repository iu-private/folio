import useful;
import geometry;
import graph;
size(10cm);

real alpha = 110;
real beta = 100;

deftriangle("MBC", triangle(radians(30), radians(30)));
point Ax = B + rotate(alpha) * (C - B);
point Dx = C + rotate(-beta) * (B - C);

circle c;
c.C = M;
c.r = length(M - C);

defpoint("A", intersectionpoints(c, line(B, Ax))[1], W);
defpoint("D", intersectionpoints(c, line(C, Dx))[1], E);
draw(A -- B -- C -- D -- cycle);



draw(segment(A, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(B, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(C, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(D, M), marker = StickIntervalMarker(n = 2, i = 1));

markangle("$\alpha$", B, A, M, n = 2, radius = markangleradius/2);
markangle("$\alpha$", M, B, A, n = 2, radius = markangleradius/2);

markangle("$\gamma$", M, C, B, n = 1, radius = markangleradius/1.2);
markangle("$\gamma$", C, B, M, n = 1, radius = markangleradius/1.2);

markangle("$\beta$", D, C, M, n = 3, radius = markangleradius/2);
markangle("$\beta$", M, D, C, n = 3, radius = markangleradius/2);

label("6", B -- C);
label("$x$", B -- M, 2S + W);
label("$x$", C -- M, 2S + E);
