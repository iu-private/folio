import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

size(10cm);

var I = toggling_intervals(minus = GrassHighlighter, in(-3), out(0));
draw(I);
