import useful;
import geometry;
import graph;
size(10cm);


draw(unitcircle);
defpoint("A", (-1, 0), SW);
defpoint("B", (1, 0), SE);
draw(A -- B);
point K = (1/sqrt(2), 1/sqrt(2));
point M = K + (0.5, -0.5);
draw(K -- M);
defaltitude("H", triangle(A, K, M).vertex(1));
defaltitude("P", triangle(B, K, M).vertex(1));
draw(H -- P);
defpoint("O", (0, 0), S);
defaltitude("K", triangle(O, H, P).vertex(1));
draw(segment(A, O), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(O, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(O, K), marker = StickIntervalMarker(i = 1, n = 2));
