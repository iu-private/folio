import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

size(10cm);

real K = 3.5;

scale(Linear(1), Linear(1/K));

xaxis("$x$", LeftTicks(format = "$\ \ \scriptstyle{}%.4g$", Step=1), EndArrow,
      above = false);
yaxis("$y$", RightTicks(format = "$\scriptstyle{}%.4g$", Step=5), EndArrow);
draw(graph(new real(real x) { return x *x - 4x + 3; }, -2.1, 3.2),
     linewidth(1.1));
draw(graph(new real(real x) { return -1; }, -2.1, 2.5));
draw(graph(new real(real x) { return -8x -1; }, -2.1, 0.2));
draw(graph(new real(real x) { return -4x + 7; }, -2, 2));
label("$(-2, 15)$", (-2, 15/K), NE);

for (real x = -2; x <= 2; x += 0.08) {
  real y1 = max(-1, -8x - 1);
  real y2 = -4x + 7;
  draw((x, y1/K) -- (x, y2/K), dotted);
}

draw((-2, 15/K) -- (-2, -1/K), dashed);
markrightangle((-2, 15/K), (-2, -1/K), (2, -1/K), size = 10, dashed);
