import useful;
import geometry;
size(10cm);
circle C1;
C1.C = (0, 0);
C1.r = 1;
circle C2;
C2.C = (0.45, -0.55);
C2.r = 0.5;
draw(C1);
draw(C2);

point E = C1.C;
point F = C2.C;
label("$E$", E, N);
label("$F$", F, 2N + plain.E);

point[] points = intersectionpoints(C1, C2);
point C = points[0];
point D = points[1];
label("$C$", C, SW);
label("$D$", D, plain.E);
point M = (C + D)/2;
label("$M$", M, S + plain.E);

draw(C -- D -- E -- cycle);
draw(C -- D -- F -- cycle, dashed);
draw(E -- M);
draw(orth(M, C, E, scale = 0.15));

clip((-0.3, -2) -- (-0.3, 0.2) -- (2, 0.2) -- (2, -2) -- cycle);

// path c1 = unitcircle;
// path c2 = shift(0.8, 0.5) * scale(0.7) * unitcircle;
// draw(c1);
// draw(c2)
