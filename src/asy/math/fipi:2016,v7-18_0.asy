import graph;
size(10cm);
xaxis("$x$", LeftTicks, EndArrow);
yaxis("$y$", LeftTicks, EndArrow);

typedef real realfunc(real);
realfunc periodical(realfunc f, real l, real r) {
  real period = r - l;
  return new real (real val) {
    while (val > r)
      val -= period;
    while (val < l)
      val += period;
    return f(val);
  };
};

realfunc mk_f(real a) {
  return periodical(new real(real x) { return a * x * x; }, -8/3, 8/3);
}

real sqrt3(real x) {
  if (x < 0) return -(abs(x) ** (1/3));
  return x ** (1/3);
}

realfunc mk_g(real a) {
  return new real(real x) { return abs(a + 2) * sqrt3(x); };
}

real a = -18/41;
realfunc f = mk_f(a);
realfunc g = mk_g(a);
draw(graph(f, -12, 12, n = 600), blue);
draw(graph(g, -12, 12, n = 600), blue);

real a = 18/23;
realfunc f = mk_f(a);
realfunc g = mk_g(a);

draw(graph(f, -12, 12, n = 600), red + dotted);
draw(graph(g, -12, 12, n = 600), red + dotted);
