import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;
size(10cm);

var I = toggling_intervals(plus = GrassHighlighter, out(0), in(1));
draw(I);
