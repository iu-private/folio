import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

size(5cm);

draw(unitcircle, grey);
draw((-1.0, 0) -- (1.0, 0));
draw((0, -1.0) -- (0, 1.0));


var circ = scale(0.035) * unitcircle;
void trig_label(real value, Label l) {
  pair p = (cos(value), sin(value));
  fill(shift(p) * circ, black);
  label(l, p, p);
};

trig_label(-pi, "$-\pi$");
trig_label(-5pi/2, "$-\frac{5\pi}{2}$");
draw(arc((0,0), 1, degrees(-5pi/2), degrees(-pi)), linewidth(1.01));
trig_label(-2pi, "$-2\pi$");
trig_label(-11pi/6, "$-\frac{11\pi}{6}$");
trig_label(-7pi/6, "$-\frac{7\pi}{6}$");
fill(shift((-1, 0)) * circ, white);
draw(shift((-1, 0)) * circ, black);
