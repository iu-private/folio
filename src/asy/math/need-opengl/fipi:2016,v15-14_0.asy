import three;
import useful;

size3(20cm);

triple A = (1,   1, 0);
triple B = (1,  -1, 0);
triple C = (-1, -1, 0);
triple D = (-1,  1, 0);
triple A1 = A + Z;
triple B1 = B + Z;
triple C1 = C + Z;
triple D1 = D + Z;
label("$A$", A);
label("$B$", B);
label("$C$", C);
label("$D$", D);

label("$A_1$", A1);
label("$B_1$", B1);
label("$C_1$", C1);
label("$D_1$", D1);

draw(B -- A -- D, green);
draw(B -- C -- D, green + dashed);
draw(A1 -- B1 --C1 --D1 -- cycle, green);
draw(A -- A1, green);
draw(B -- B1, green);
draw(C -- C1, green + dashed);
draw(D -- D1, green);

triple K = (A + B)/2;
triple M = (B1 + C1)/2;
triple L = (A + D)/2;
label("$K$", K);
label("$M$", M);
label("$L$", L);
draw(K -- L, dashed);

triple P = B + 0.5(B - C);
label("$P$", P);
draw(K --P -- B);
triple Q = (B + B1)/2;
label("$Q$", Q);
draw(P -- Q);
draw(Q -- M, dashed);
draw(Q -- K);
triple R = (C1 + D1)/2;
label("$R$", R);
draw(M -- R);
triple T = D + 0.5(D-C);
label("$T$", T);
draw(L -- T -- D);
triple V = (D + D1)/2;
label("$V$", V);
draw(R -- V, dashed);
draw(T -- V);
draw(L -- V);

draw(B -- A1 -- D, blue);
draw(B -- D, dashed + blue);
triple F1 = (Q + K)/2;
triple F2 = (V + L)/2;
label("$F_1$", F1, N);
label("$F_2$", F2, N);
draw(F1 -- F2, blue + dotted + linewidth(1.2));
triple O = (0, 0, 1/2);
draw(M -- L, dotted);
draw(Q -- V, dotted);
label("$O$", O);
triple H = (F1 + F2)/2;
label("$H$", H);
draw(A1 -- O -- H -- cycle, red + dashed);
