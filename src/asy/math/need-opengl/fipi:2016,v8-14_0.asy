import three;
import useful;

size3(10cm);

triple B = (1.5, 1.5, 0);
triple C = (1.5, -1.5, 0);
triple D = (-1.5, -1.5, 0);
triple A = (-1.5, 1.5, 0);
triple h = (0, 0, 4);

triple A1 = A + h;
triple B1 = B + h;
triple C1 = C + h;
triple D1 = D + h;


draw(A -- B -- C -- D -- cycle, dashed);
draw(A1 -- B1 -- C1 -- D1 -- cycle, dashed);
draw(A -- A1, dashed);
draw(B -- B1, dashed);
draw(C -- C1, dashed);
draw(D -- D1, dashed);
label("$A$", A, S);
label("$B$", B, W);
label("$C$", C, NW);
label("$D$", D, E);
label("$A_1$", A1, N);
label("$B_1$", B1, W);
label("$C_1$", C1, N);
label("$D_1$", D1, E);
draw(C -- C1 -- B1 -- B -- cycle);
draw(B -- A -- A1 -- B1 -- cycle);

currentprojection = perspective(8,6,1.5);

triple E = (3 * A1 + A)/4;
label("$E$", E, SE);
draw(B -- E);
draw(D1 -- E, dashed);
triple R = intersectionpoint(C -- C1, D1 -- (D1 + (B - E)));
draw(D1 -- R, dashed);
label("$R$", R, plain.E);
draw(R -- B);
triple Q = intersectionpoint(D1 -- (D1 + 8(R - D1)), D -- (D + 8(C - D)));
draw(R -- Q);
draw(C -- Q);
label("$Q$", Q, S);
draw(B -- Q);

triple V = (3Q + B)/4;
label("$V$", V, S);
draw(R -- C -- V);

