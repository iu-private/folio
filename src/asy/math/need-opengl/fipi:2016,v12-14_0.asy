import three;
import useful;
size(10cm);

triple A = (-3, -3, 0);
triple B = (-3, 3, 0);
triple C = (3, 3, 0);
triple D = (3, -3, 0);
triple S = (0, 0, sqrt(81 - 18));

draw(A -- B -- C -- D -- cycle);
draw(A -- S -- B -- cycle);
draw(B -- S -- C -- cycle);
draw(C -- S -- D -- cycle);
draw(D -- S -- A -- cycle);
label("$A$", A, plain.S);
label("$B$", B, E);
label("$C$", C, SE);
label("$D$", D, W);
label("$S$", S, N);
currentprojection = perspective(5, 3, 4);
triple M = (2S + A)/3;
label("$M$", M, E);
triple K = (2S + D)/3;
label("$K$", K, W);
draw(B -- M, dashed);
draw(C -- K);
draw(M -- K, dashed);

triple U = intersectionpoint(B -- (B + 20(M - B)), C -- (C + 20(K - C)));
draw(K -- U);
draw(M -- U);
label("$U$", U, N);
draw(S -- U);
triple V = (B + C)/2;
draw(U -- V, dotted);
label("$V$", V, SE);
triple h = unit(cross(U - V, K - M));
// Somewhy intersection fails
triple H = S - 2.4h;
draw(S -- H);
draw(orth(H, S, V, scale = 0.15));
label("$H$", H, 2N);
draw(S -- O -- V -- cycle);
draw(orth(O, S, V));
label("$O$", O, W);

