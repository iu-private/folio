import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

real a = 3;
real b = 12;
real phi = pi/4;
real h = 6 * sin(phi);

point A = (0, 0);
point B = (12 - 6cos(phi), h);
point C = (15 - 6cos(phi), h);
point D = (12, 0);
quadrilateral tr = quadrilateral(A, B, C, D);
draw(B -- D);
markangle(B, D, A);
markangle(D, B, C);
draw(tr);
