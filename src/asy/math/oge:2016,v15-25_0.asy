import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

deftriangle("ABC", triangle(pi/6, 2pi/3));
point A1 = intersectionpoint(line(B, C), altitude(ABC.vertex(1)));
draw(A -- A1);
label("$A_1$", A1, S);

point B1 = intersectionpoint(line(A, C), altitude(ABC.vertex(2)));
draw(B -- B1);
label("$B_1$", B1, S);
draw(A1 -- B1 -- C -- cycle, dashed);
markrightangle(A, A1, C);
markrightangle(B, B1, C);
markangle(B, C, B1);
markangle(A1, C, A);
markangle(A, C, B, n = 2, radius = markangleradius/1.4);
markangle(B1, C, A1, n = 2, radius = markangleradius/1.4);




// quadrilateral tr = parallelogram(pi/3, 1);
// unravel tr;
// draw(tr);
