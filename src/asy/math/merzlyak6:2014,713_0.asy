import useful;
import geometry;
import graph;
size(10cm);
point point(real alpha)
{
  return (cos(alpha), sin(alpha));
}

defpoint("A", point(0.0), E);
defpoint("B", point(2pi/3), NW);
defpoint("C", point(4pi/3), SW);
draw(A -- B -- C -- cycle);
draw(unitcircle);
