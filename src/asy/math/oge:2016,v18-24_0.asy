import useful;
import geometry;
import graph;
size(10cm);

real a = 10;
real b = 24;

triangle t = triangle(atan(b/a), pi/2);
deftriangle("ABC", t);
markrightangle(A, C, B);

defaltitude("H", t, 3);
label(string(b), A -- C);
label(string(a), B -- C);
