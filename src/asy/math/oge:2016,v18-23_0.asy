import useful;
import geometry;
import graph;
size(10cm);

xaxis("$x$", RightTicks(Step = 2), EndArrow);
yaxis("$y$", RightTicks(Step = 4), EndArrow);

real xl = -8.2;
real xr = 3;

draw(graph(new real(real x) { return x^2 - 2x; }, 0, xr));
draw(graph(new real(real x) { return -x^2 - 8x; }, xl, 0));
path p = (xl, -1) -- (xr, -1);
draw(p, linewidth(1.2));
label("$y = -1$", p, 2S);

path p = (xr, 16) -- (xl, 16);
draw(p, linewidth(1.2));
label("$y = 16$", p, 10W + 2N);
