import useful;
import geometry;
import graph;
size(10cm);

point midpoint(point A, point B)
{
  return (A + B)/2;
}

deftriangle("ACB", triangleabc(5, 5, 6));
defpoint("M", midpoint(A, B), 2W);
defpoint("N", midpoint(B, C), 2E);
draw(M -- N);
label("3", M -- N);
label("6", A -- C);
