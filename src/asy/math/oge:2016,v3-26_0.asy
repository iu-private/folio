import geometry;
import useful;

size(10cm);

real a = 12;
real b = 30; // Right value is 36, but with b = 30 picture is prettier
real r = 13;

real phi = acos(r/(b - a));
real h = r * sin(phi);
real bx = r * cos(phi);

defpoint("A", (0, 0), W);
defpoint("D", (b, 0), E);
defpoint("B", (bx, h), NW);
defpoint("C", (bx + a, h), 2N);

draw(A -- B -- C -- D -- cycle);

label("$13$", A -- B, 2E);
label("$12$", B -- C, 2N);
label("$36$", A -- D, S);

point U = intersectionpoint(line(A, B), line(C, D));
label("$U$", U, N);
draw(B -- U -- C, dashed);
markrightangle(A, U, D);
real l = r * sqrt(a * b)/(b - a);
defpoint("T", U + l * unit(D - U), 2E);
draw(circle(A, B, T));

draw(A -- T -- B -- cycle);
markangle(T, A, B, radius = markangleradius/2);
markangle(U, T, B, radius = markangleradius/2);
point A_ = A - (2, 2);
point D_ = D + (2, -2);
point U_ = U + (0, 2);
clip(A_ -- (A_.x, U_.y) -- (D_.x, U_.y) -- D_ -- cycle);
