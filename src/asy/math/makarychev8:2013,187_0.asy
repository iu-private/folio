import useful;
import graph;
size(10cm);

xaxis(LeftTicks(Step = 1));
yaxis(RightTicks(Step = 1));


real fn(real x) { return 8/x; }

draw(graph(new real(real x) { return x^2; }, -2.1, 2.1), red  + linewidth(1.15));
draw(graph(new real(real x) { return x^3; }, -1.8, 1.8), blue);
draw(graph(fn, 1.2, 6), linewidth(1.3));
draw(graph(fn, -6, -1.2), linewidth(1.3));

for (real x: new real[] { 8^0.25, -8^0.25, 2 }) {
  draw((x, 0) -- (x, 8/x) -- (0, 8/x), dashed);
}
